
In this project we have implemented a multiplayer tic tac toe game using socket programming in C in which a server creates the game in local area network. The players in LAN can connect to the server by using the IP address if the server. 
We have used the concept of thread.When a new connection arrives ,new thread will be created.
 To avoid race condition taking place between threads we have implemented the concept of mutex.
  Rules of the game:One by one the players have to enter a key ,‘0’ or ‘X’.A player can win the game when either of the diagonals have the same key i.e. ‘0’ or ‘x’ or any of the rows or the columns have the same key otherwise the game will result in a draw.
  If more than 2 players come than new board will be created for another player automatically and will wait till new player another player joins i.e for all two players groups their will be one board for each group of players.  